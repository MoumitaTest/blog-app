package com.blog_testapp.blogapplications.networking


import com.blog_testapp.blogapplications.model.BlogArryList
import com.blog_testapp.blogapplications.model.BlogResponse

import java.util.ArrayList

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BlogApi {
    @GET("/blogs")
    fun getBlogList(@Query("page") pageNo: String,
                    @Query("limit") limitCount: String): Call<ArrayList<BlogResponse>>
}
