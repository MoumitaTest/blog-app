package com.blog_testapp.blogapplications.networking


import androidx.lifecycle.MutableLiveData

import com.blog_testapp.blogapplications.model.BlogArryList
import com.blog_testapp.blogapplications.model.BlogResponse

import java.util.ArrayList

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BlogRepository {
    private val blogApi: BlogApi

    init {
        blogApi = RetrofitService.cteateService(BlogApi::class.java)
    }

    fun getBlogData(pageNo: String, limitCount: String): MutableLiveData<ArrayList<BlogResponse>> {

        val blogData = MutableLiveData<ArrayList<BlogResponse>>()
        blogApi.getBlogList(pageNo, limitCount).enqueue(object : Callback<ArrayList<BlogResponse>> {
            override fun onResponse(call: Call<ArrayList<BlogResponse>>, response: Response<ArrayList<BlogResponse>>) {
                if (response.isSuccessful) {
                    blogData.value = response.body()
                }
            }

            override fun onFailure(call: Call<ArrayList<BlogResponse>>, t: Throwable) {
                blogData.value = null
            }
        })
        return blogData
    }

    companion object {

        private var blogRepository: BlogRepository? = null

        val instance: BlogRepository
            get() {
                if (blogRepository == null) {
                    blogRepository = BlogRepository()
                }
                return blogRepository!!
            }
    }
}
