package com.blog_testapp.blogapplications.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()


    fun <S> cteateService(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }

}
