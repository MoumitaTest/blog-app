package com.blog_testapp.blogapplications

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.blog_testapp.blogapplications.adapters.BlogAdapter
import com.blog_testapp.blogapplications.model.BlogResponse
import com.blog_testapp.blogapplications.viewmodels.BlogViewModel

import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    internal var articleArrayList = ArrayList<BlogResponse>()
    internal lateinit var blogAdapter: BlogAdapter
    internal lateinit var rvHeadline: RecyclerView
    internal lateinit var blogViewModel: BlogViewModel
    internal lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rvHeadline = findViewById(R.id.rvNews)
        layoutManager = LinearLayoutManager(this@MainActivity)
        rvHeadline.layoutManager = layoutManager
        rvHeadline.setHasFixedSize(true)

        blogViewModel = ViewModelProviders.of(this).get(BlogViewModel::class.java)
        blogViewModel.init()
            blogViewModel.getBlogRepository()?.observe(this, Observer<ArrayList<BlogResponse>> {
            articleArrayList.addAll(it)
            //blogAdapter.notifyDataSetChanged();
            setupRecyclerView()
        })


    }

    private fun setupRecyclerView() {
        //if (blogAdapter == null) {
        blogAdapter = BlogAdapter(this@MainActivity, articleArrayList)
        rvHeadline.layoutManager = LinearLayoutManager(this)
        rvHeadline.adapter = blogAdapter
        rvHeadline.itemAnimator = DefaultItemAnimator()
        rvHeadline.isNestedScrollingEnabled = true
        blogAdapter.notifyDataSetChanged()
        // } else {
        //   blogAdapter.notifyDataSetChanged();
        //}
    }

}
