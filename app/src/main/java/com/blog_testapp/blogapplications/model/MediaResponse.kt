package com.blog_testapp.blogapplications.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MediaResponse {
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null

    override fun toString(): String {
        return "MediaResponse{" +
                "image='" + image + '\''.toString() +
                ", url='" + url + '\''.toString() +
                '}'.toString()
    }
}
