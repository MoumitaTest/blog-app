package com.blog_testapp.blogapplications.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserResponse {
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("lastname")
    @Expose
    var lastname: String? = null
    @SerializedName("designation")
    @Expose
    var designation: String? = null
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null

    override fun toString(): String {
        return "UserResponse{" +
                "name='" + name + '\''.toString() +
                ", lastname='" + lastname + '\''.toString() +
                ", designation='" + designation + '\''.toString() +
                ", avatar='" + avatar + '\''.toString() +
                '}'.toString()
    }
}
