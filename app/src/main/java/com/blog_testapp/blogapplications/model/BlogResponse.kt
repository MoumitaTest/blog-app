package com.blog_testapp.blogapplications.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class BlogResponse {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("content")
    @Expose
    var content: String? = null
    @SerializedName("comments")
    @Expose
    var comments: String? = null
    @SerializedName("likes")
    @Expose
    var likes: String? = null

    @SerializedName("user")
    @Expose
    var userResponses: ArrayList<UserResponse>? = null

    @SerializedName("media")
    @Expose
    var mediaResponses: ArrayList<MediaResponse>? = null

    override fun toString(): String {
        return "BlogResponse{" +
                "id='" + id + '\''.toString() +
                ", content='" + content + '\''.toString() +
                ", comments='" + comments + '\''.toString() +
                ", likes='" + likes + '\''.toString() +
                ", userResponses=" + userResponses +
                ", mediaResponses=" + mediaResponses +
                '}'.toString()
    }
}
