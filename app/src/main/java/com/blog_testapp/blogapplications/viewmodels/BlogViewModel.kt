package com.blog_testapp.blogapplications.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.blog_testapp.blogapplications.model.BlogArryList
import com.blog_testapp.blogapplications.model.BlogResponse
import com.blog_testapp.blogapplications.networking.BlogRepository

import java.util.ArrayList


class BlogViewModel : ViewModel() {
    var mutableLiveData: MutableLiveData<ArrayList<BlogResponse>>? = null
    internal var blogRepository: BlogRepository? = null

    fun init() {
        if (mutableLiveData != null) {
            return
        }
        blogRepository = BlogRepository.instance
        mutableLiveData = blogRepository!!.getBlogData("1", "10")

    }

    fun getBlogRepository(): LiveData<ArrayList<BlogResponse>>? {
        return mutableLiveData
    }

}
