package com.blog_testapp.blogapplications.adapters

import android.content.Context
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.blog_testapp.blogapplications.R
import com.blog_testapp.blogapplications.model.BlogResponse
import com.squareup.picasso.Picasso

import java.util.ArrayList

class BlogAdapter(internal var context: Context, internal var articles: ArrayList<BlogResponse>) : RecyclerView.Adapter<BlogAdapter.BlogViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.blog_item, parent, false)
        return BlogViewHolder(view)
    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        try {
            holder.tvName.text = articles[position].userResponses!![0]?.name + "  " + articles[position].userResponses!![0].lastname
            holder.tvDesignation.text = articles[position].userResponses!![0].designation
            holder.tvDesCription.text = articles[position].content
            holder.tvURL.text = articles[position].mediaResponses!![0].url
            holder.tvURL.movementMethod = LinkMovementMethod.getInstance()
            val Likes = articles[position].likes?.let { Integer.parseInt(it) }
            val comments = articles[position].comments?.let { Integer.parseInt(it) }
            var numberString = ""
            if (Likes != null) {
                if (Math.abs(Likes / 1000000) > 1) {
                    numberString = (Likes / 1000000).toString() + "m"
                    holder.tvLike.text = "$numberString Likes"
                } else if (Math.abs(Likes / 1000) > 1) {
                    numberString = (Likes / 1000).toString() + "k"
                    holder.tvLike.text = "$numberString Likes"
                } else {
                    numberString = Likes.toString()
                    holder.tvLike.text = "$numberString Likes"
                }
            }
            var numberStringforComments = ""
            if (comments != null) {
                if (Math.abs(comments / 1000000) > 1) {
                    numberStringforComments = (comments / 1000000).toString() + "m"
                    holder.tvComment.text = "$numberStringforComments Comments"
                } else if (Math.abs(comments / 1000) > 1) {
                    numberStringforComments = (comments / 1000).toString() + "k"
                    holder.tvComment.text = "$numberStringforComments Comments"
                } else {
                    numberStringforComments = comments.toString()
                    holder.tvComment.text = "$numberStringforComments Comments"
                }
            }

            Picasso.get().load(articles[position].mediaResponses!![0].image).into(holder.ivNews)
            Picasso.get().load(articles[position].userResponses!![0].avatar).into(holder.ivAvatar)
        } catch (e: Exception) {
            Log.e("Exception $position", e.toString())
        }

    }

    override fun getItemCount(): Int {
        return articles.size
    }

    inner class BlogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var tvName: TextView
        internal var tvDesCription: TextView
        internal var tvComment: TextView
        internal var tvLike: TextView
        internal var tvURL: TextView
        internal var tvDesignation: TextView
        internal var ivNews: ImageView
        internal var ivAvatar: ImageView

        init {

            tvName = itemView.findViewById(R.id.tvName)
            tvDesCription = itemView.findViewById(R.id.tvDesCription)
            tvComment = itemView.findViewById(R.id.tvComment)
            tvLike = itemView.findViewById(R.id.tvLike)
            tvDesignation = itemView.findViewById(R.id.tvDesignation)
            tvURL = itemView.findViewById(R.id.tvURL)
            ivNews = itemView.findViewById(R.id.ivNews)
            ivAvatar = itemView.findViewById(R.id.ivAvatar)

        }
    }
}
